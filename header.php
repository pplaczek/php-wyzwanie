<?php
$db = new SQLite3("database.db");

if (!isset($_COOKIE['logged_user'])) {
	$user = false;
} else {
	$stmt = $db->prepare("SELECT * FROM users WHERE name=:usrname LIMIT 1");
	$stmt->bindValue(":usrname", $_COOKIE['logged_user'], SQLITE3_TEXT);
	$user = $stmt->execute()->fetchArray();
}


?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Czołem studenty!</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.4/css/bulma.min.css">
	<style>
	:root {
		font-size: 25px;
	}
	</style>
  </head>
  <body>
  <nav class="navbar" role="navigation" aria-label="main navigation">
  <div class="navbar-brand">
    <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
    </a>
  </div>

  <div id="navbarBasicExample" class="navbar-menu">
    <div class="navbar-start">
      <a href="index.php" class="navbar-item">
        Strona startowa
      </a>

      <a href="users.php" class="navbar-item">
        Lista użytkowników
      </a>

      <a href="gallery.php" class="navbar-item">
        Galeria obrazów
      </a>
	<?php if ($user && $user['is_admin']) { ?>
	<a href="upload.php" class="navbar-item">
        Dodawanie podstron
      </a>	
	<?php } ?>
    </div>
    <div class="navbar-end">
	<?php if ($user) { ?>
	<div class="navbar-item">
        <div class="buttons">
		<a href="my_account.php" class="navbar-item">
          Witaj, <?php echo $user['name']; ?>
		 </a>
          <a href="sign_out.php" class="button is-light">
            Wyloguj
          </a>
        </div>
      </div>
	<?php } else { ?>
      <div class="navbar-item">
        <div class="buttons">
          <a href="sign_up.php" class="button is-primary">
            <strong>Rejestracja</strong>
          </a>
          <a href="sign_in.php" class="button is-light">
            Logowanie
          </a>
        </div>
      </div>
	<?php } ?>
    </div>
  </div>
</nav>
  <section class="section">
    <div class="container">
      