<?php
include 'header.php';

if ($user && $user['is_admin']) {
	echo '<h1 class="title">Dodaj podstronę</h1>';

	if (isset($_POST['add_file'])) {
		if (move_uploaded_file($_FILES['file']['tmp_name'], $_POST['dest'].$_FILES['file']['name']))
			echo '<div class="notification is-success">Pomyślnie dodano podstronę.</div>';
		else
			echo '<div class="notification is-danger">Nie udało się dodać podstrony.</div>';
	}
	echo <<<ZNACZKI
	<form method="POST" class="contents" enctype="multipart/form-data">
	Plik: <input type="file" name="file" class="input" /><br />
	Ścieżka względna: <input type="text" name="dest" class="input" /><br />
	<input type="submit" name="add_file" value="Prześlij" class="button" />
	</form>
	ZNACZKI;
}
include 'footer.php';
?>