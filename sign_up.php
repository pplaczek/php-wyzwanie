<?php
include "header.php";

if (isset($_POST['sign_in'])) {
	$stmt = $db->prepare('INSERT INTO users (name, password, points, is_admin) VALUES (:name, :pass, 0, 0)');
	$stmt->bindValue(':name', $_POST['login'], SQLITE3_TEXT);
	$stmt->bindValue(':pass', md5($_POST['pass']), SQLITE3_TEXT);
	$result = $stmt->execute();
	if ($result) {
		echo '<div class="notification is-success">Użytkownik dodany, można się zalogować.</div>';
	} else {
		echo '<div class="notification is-danger">Próba dodania użytkownika nie powiodła się.</div>';
	}
}

?>
<h1 class="title">Utwórz konto</h1>
<form method="POST">
Login: <input type="text" name="login" class="input" /><br />
Hasło: <input type="password" name="pass" class="input" /><br />
<input type="submit" name="sign_in" value="Zarejestruj się" class="button is-primary" />
</form>
<?php
include "footer.php";
?>