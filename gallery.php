<?php
include 'header.php';

echo '<h1 class="title">Galeria obrazów</h1>';

if (isset($_POST['add_image'])) {
	for ($i = 0; $i < count($_FILES['image']['tmp_name']); $i++) {
		if (move_uploaded_file($_FILES['image']['tmp_name'][$i], 'uploads/'.$_FILES['image']['name'][$i]))
			echo '<div class="notification is-success">Pomyślnie dodano obraz.</div>';
		else
			echo '<div class="notification is-danger">Nie udało się dodać obrazu.</div>';
	}
}
echo <<<ZNACZKI
<form method="POST" class="contents" enctype="multipart/form-data">
<h2>Dodaj obrazy</h2>
Plik obrazu: <input type="file" name="image[]" class="input" multiple /><br />
<input type="submit" name="add_image" value="Prześlij obrazy" class="button" />
</form>
ZNACZKI;


echo '<div class="tile is-ancestor is-flex-wrap-wrap">';

$dir = opendir('./uploads');
while ($d = readdir($dir)) {
	if (!in_array($d, array('index.php', '.', '..')))
		echo <<<EOS
	<div class="tile is-parent is-2">
		<article class="tile is-child box">
			<img src='./uploads/{$d}' />
		</article>
	</div>
	EOS;
}

echo '</div>';

include 'footer.php';
?>