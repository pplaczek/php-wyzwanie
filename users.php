<?php

include "header.php";

/*
$db->query("CREATE TABLE IF NOT EXISTS users  (
	id INTEGER NOT NULL PRIMARY KEY,
	name TEXT NOT NULL,
	password TEXT NOT NULL,
	points INTEGER NOT NULL,
	is_admin INTEGER NOT NULL DEFAULT 0
)");
*/

if (isset($_POST['filter'])) {
	$_POST['username'] = str_replace('"', "", $_POST['username']);
	if ($_POST['points'] && $_POST['username'])
		$results = $db->query("SELECT * FROM users WHERE name LIKE \"%{$_POST['username']}%\" AND points > {$_POST['points']} AND is_admin = 0");
	elseif ($_POST['points'])
		$results = $db->query("SELECT * FROM users WHERE points > {$_POST['points']} AND is_admin = 0");
	elseif ($_POST['username'])
		$results = $db->query("SELECT * FROM users WHERE name LIKE \"%{$_POST['username']}%\" AND is_admin = 0");
	else
		$results = $db->query("SELECT * FROM users WHERE is_admin = 0");
} else {
	$results = $db->query("SELECT * FROM users WHERE is_admin = 0");
}

echo <<<EOF
<form method="POST">
	<div class="field has-addons has-addons-centered">
	  <p class="control">
		  <input type="text" name="username" placeholder="Nazwa użytkownika" class="input" />
	  </p>
	  <p class="control">
		<input type="number" name="points" placeholder="Minimalna liczba punktów" class="input" />
	  </p>
	  <p class="control">
		<input type="submit" name="filter" value="Filtruj wyniki" class="button is-info" />
	  </p>
	</div>	
</form>
EOF;

echo '<table class="table is-hoverable mx-auto">
<tr><th>Id użytkownika</th><th>Nazwa użytkownika</th><th>Punkty</th></tr>';
while ($row = $results->fetchArray()) {
	echo "<tr><td>{$row['id']}</td><td>{$row['name']}</td><td>{$row['points']}</td></tr>";
}
echo "</table>";

include "footer.php";

?>