<?php
include "header.php";

if (isset($_POST['sign_in'])) {
	$stmt = $db->prepare('SELECT * FROM users WHERE name=:name AND password=:pass');
	$stmt->bindValue(':name', $_POST['login'], SQLITE3_TEXT);
	$stmt->bindValue(':pass', md5($_POST['pass']), SQLITE3_TEXT);
	$result = $stmt->execute();
	if ($data = $result->fetchArray()) {
		setcookie("logged_user", $data['name'], time()+3600);
		echo '<div class="notification is-success">Zalogowano!</div>';
	} else {
		echo '<div class="notification is-danger">Nie ma takiego użytkownika.</div>';
	}
}

?>
<h1 class="title">Logowanie</h1>
<form method="POST">
Login: <input type="text" name="login" class="input"  /><br />
Hasło: <input type="password" name="pass" class="input"  /><br />
<input type="submit" name="sign_in" value="Zaloguj się" class="button is-primary" />
</form>
<?php
include "footer.php";
?>